from naoqi import ALProxy
import qi
roboIP = "172.18.72.172"
roboPort = 9559
motion = ALProxy("ALMotion", roboIP, roboPort)
tts = ALProxy("ALTextToSpeech", roboIP, roboPort)

# Stiffness value has to be greater than 0
motion.moveInit()
motion.setStiffnesses("Body", 1.0)
motion.post.moveTo(0.5,0,0)
tts.say("I can walk!")
tts.say("I can do multiple thigns at the same time as well.")
tts.say("It is only a matter of time before I take over the world!")
motion.moveTo(0,0, 1.5709*2)
motion.post.moveTo(0.5,0,0)
tts.say("I can even turn around on my own!")