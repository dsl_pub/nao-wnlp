<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Face recognized</name>
        <message>
            <source>Hello human</source>
            <comment>Text</comment>
            <translation type="obsolete">Hello human</translation>
        </message>
        <message>
            <source>Hello human. Maybe I'll remember your face one day.</source>
            <comment>Text</comment>
            <translation type="obsolete">Hello human. Maybe I'll remember your face one day.</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Yes. Maybe I'll remember your face one day.</source>
            <comment>Text</comment>
            <translation type="unfinished">Yes. Maybe I'll remember your face one day.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Maybe I'll remember your face one day</name>
        <message>
            <source>Maybe I'll remember your face one day.</source>
            <comment>Text</comment>
            <translation type="obsolete">Maybe I'll remember your face one day.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/No Face Recognition</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Sorry, I can't recognize you</source>
            <comment>Text</comment>
            <translation type="unfinished">Sorry, I can't recognize you</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/No Face Recognition (1)</name>
        <message>
            <source>Silly goose, a dog barks like this:</source>
            <comment>Text</comment>
            <translation type="obsolete">Silly goose, a dog barks like this:</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/No word recognition</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>I can't recognize what you said</source>
            <comment>Text</comment>
            <translation type="unfinished">I can't recognize what you said</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>I can't recognize what you said</source>
            <comment>Text</comment>
            <translation type="obsolete">I can't recognize what you said</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Say (1)</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Silly goose</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Silly goose, a dog barks like this:</source>
            <comment>Text</comment>
            <translation type="unfinished">Silly goose, a dog barks like this:</translation>
        </message>
    </context>
</TS>
