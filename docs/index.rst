.. NAO documentation master file, created by
   sphinx-quickstart on Sun Feb 16 20:59:28 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NAO's documentation!
===============================

`NAO`_ is a friendly robot that can be be programmed to do various things such as speak, walk, and even identify
objects or people as well as do many other tasks. It is the first robot created by SoftBank Robotics. 


.. _NAO: https://www.softbankrobotics.com/emea/en/nao
.. toctree::
   :maxdepth: 2
   :caption: Getting Started with NAO:



Getting Started with NAO
==================

This project is being created using Python and Choregraphe.

* :doc:`Python <python>`
* :doc:`Choregraphe <choregraphe>`
* :doc:`Errors <errors>`
