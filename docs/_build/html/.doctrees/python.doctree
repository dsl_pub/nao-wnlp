��Dp      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Python�h]�h �Text����Python�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�Sc:\Users\Chick3n\Desktop\ECU Spring 2020\NAO Robot Project\nao-wnlp\docs\python.rst�hKubh �	paragraph���)��}�(h�eNAO robot works with Python 2.7. This guide will walk you through the
installation process of Python.�h]�h�eNAO robot works with Python 2.7. This guide will walk you through the
installation process of Python.�����}�(hh/hh-hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh �comment���)��}�(hX�  The directive toctree lets us combine multiple pages into a
cohesive hierarchy. It outputs a Table of Contents in the
page where it occurs. The maxdepth option/argument tells
Sphinx to include 2 levels of headers. It lets Sphinx
know that the other pages are sub-pages of the current page,
creating a "tree" structure of the pages:
Python
├── Installation
├── Setting the Environment Variable path
For more information, visit,
https://www.ericholscher.com/blog/2016/jul/1/sphinx-and-rtd-for-writers/�h]�hX�  The directive toctree lets us combine multiple pages into a
cohesive hierarchy. It outputs a Table of Contents in the
page where it occurs. The maxdepth option/argument tells
Sphinx to include 2 levels of headers. It lets Sphinx
know that the other pages are sub-pages of the current page,
creating a "tree" structure of the pages:
Python
├── Installation
├── Setting the Environment Variable path
For more information, visit,
https://www.ericholscher.com/blog/2016/jul/1/sphinx-and-rtd-for-writers/�����}�(hhhh=ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve�uh)h;hhhhhh*hKubh �compound���)��}�(hhh]��sphinx.addnodes��toctree���)��}�(hhh]�h}�(h]�h!]�h#]�h%]�h']�h�python��entries�]��includefiles�]��maxdepth�K�caption�N�glob���hidden���includehidden���numbered�K �
titlesonly���
rawentries�]�uh)hShh*hKhhOubah}�(h]�h!]��toctree-wrapper�ah#]�h%]�h']�uh)hMhhhhhh*hNubh
)��}�(hhh]�(h)��}�(h�Installation�h]�h�Installation�����}�(hhxhhvhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhhshhhh*hKubh,)��}�(h��You can download the python programming language from
`Python's <https://www.python.org/downloads/>`_ official website. For this
project, Python version 2.7.17 32-bit was used.�h]�(h�6You can download the python programming language from
�����}�(h�6You can download the python programming language from
�hh�hhhNhNubh �	reference���)��}�(h�/`Python's <https://www.python.org/downloads/>`_�h]�h�
Python’s�����}�(h�Python's�hh�ubah}�(h]�h!]�h#]�h%]�h']��name�h��refuri��!https://www.python.org/downloads/�uh)h�hh�ubh �target���)��}�(h�$ <https://www.python.org/downloads/>�h]�h}�(h]��python-s�ah!]�h#]��python's�ah%]�h']��refuri�h�uh)h��
referenced�Khh�ubh�K official website. For this
project, Python version 2.7.17 32-bit was used.�����}�(h�K official website. For this
project, Python version 2.7.17 32-bit was used.�hh�hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK hhshhubh,)��}�(h�cFor a quick download: `Python 2.7.17 <https://www.python.org/ftp/python/2.7.17/python-2.7.17.msi>`_�h]�(h�For a quick download: �����}�(h�For a quick download: �hh�hhhNhNubh�)��}�(h�M`Python 2.7.17 <https://www.python.org/ftp/python/2.7.17/python-2.7.17.msi>`_�h]�h�Python 2.7.17�����}�(h�Python 2.7.17�hh�ubah}�(h]�h!]�h#]�h%]�h']��name��Python 2.7.17�h��:https://www.python.org/ftp/python/2.7.17/python-2.7.17.msi�uh)h�hh�ubh�)��}�(h�= <https://www.python.org/ftp/python/2.7.17/python-2.7.17.msi>�h]�h}�(h]��python-2-7-17�ah!]�h#]��python 2.7.17�ah%]�h']��refuri�h�uh)h�h�Khh�ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK$hhshhubh �warning���)��}�(h�5**Note that a 32-bit version of Python is REQUIRED.**�h]�h,)��}�(hh�h]�h �strong���)��}�(hh�h]�h�1Note that a 32-bit version of Python is REQUIRED.�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK&hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hhshhhh*hNubh,)��}�(h�0I suggest installing Python in the default path:�h]�h�0I suggest installing Python in the default path:�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK(hhshhubh �literal_block���)��}�(h�C:\Python27.�h]�h�C:\Python27.�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']��force���highlight_args�}�hKhL�language��console�uh)j  hh*hK*hhshhubeh}�(h]��installation�ah!]�h#]��installation�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�%Setting the Environment Variable path�h]�h�%Setting the Environment Variable path�����}�(hj>  hj<  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj9  hhhh*hK/ubh,)��}�(h��While installing python, an option to set the environment variable path will be
available. This is optional but if you choose to set it, you can run
Python from the cmd simply by opening your command prompt and
typing:�h]�h��While installing python, an option to set the environment variable path will be
available. This is optional but if you choose to set it, you can run
Python from the cmd simply by opening your command prompt and
typing:�����}�(hjL  hjJ  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK1hj9  hhubj  )��}�(h�C:\Users\YourUserName>python�h]�h�C:\Users\YourUserName>python�����}�(hhhjX  ubah}�(h]�h!]�h#]�h%]�h']��force���highlight_args�}�hKhLj/  �console�uh)j  hh*hK6hj9  hhubh,)��}�(h�-Then you should see something similar appear:�h]�h�-Then you should see something similar appear:�����}�(hjl  hjj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK:hj9  hhubj  )��}�(h��C:\Users\YourUserName>python
Python 2.7.17 (v2.7.17:c2f86d86e6, Oct 19 2019, 20:49:36) [MSC v.1500 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>�h]�h��C:\Users\YourUserName>python
Python 2.7.17 (v2.7.17:c2f86d86e6, Oct 19 2019, 20:49:36) [MSC v.1500 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>�����}�(hhhjx  ubah}�(h]�h!]�h#]�h%]�h']��force���highlight_args�}�hKhLj/  �console�uh)j  hh*hK<hj9  hhubh,)��}�(h�'Now you are ready to program in python.�h]�h�'Now you are ready to program in python.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKChj9  hhubh,)��}�(h�{If you choose to not set the path through the installation, you can do it
manually by navigating to the following location:�h]�h�{If you choose to not set the path through the installation, you can do it
manually by navigating to the following location:�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKEhj9  hhubj  )��}�(h�|Control Panel >> System (Under System and Security if not in icon mode) >> Advanced
System Settings >> Environment Variables�h]�h�|Control Panel >> System (Under System and Security if not in icon mode) >> Advanced
System Settings >> Environment Variables�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']��force���highlight_args�}�hKhLj/  �console�uh)j  hh*hKHhj9  hhubh,)��}�(h��Once you are at the Environment Variables window, you should have a User variable
called Path. Either select it and hit the Edit button or double click it.
Once the edit window opens, hit the Edit button or double click an empty line
and type:�h]�h��Once you are at the Environment Variables window, you should have a User variable
called Path. Either select it and hit the Edit button or double click it.
Once the edit window opens, hit the Edit button or double click an empty line
and type:�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKMhj9  hhubj  )��}�(h�C:\Python27�h]�h�C:\Python27�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']��force���highlight_args�}�hKhLj/  �console�uh)j  hh*hKRhj9  hhubh,)��}�(h�or�h]�h�or�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKVhj9  hhubj  )��}�(h�C:\path\to\your\python\here�h]�h�C:\path\to\your\python\here�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']��force���highlight_args�}�hKhLj/  �console�uh)j  hh*hKXhj9  hhubh,)��}�(h�kFor the official Python documentation visit: `Python Documentation
<https://docs.python.org/2/index.html>`_�h]�(h�-For the official Python documentation visit: �����}�(h�-For the official Python documentation visit: �hj�  hhhNhNubh�)��}�(h�>`Python Documentation
<https://docs.python.org/2/index.html>`_�h]�h�Python Documentation�����}�(h�Python Documentation�hj  ubah}�(h]�h!]�h#]�h%]�h']��name��Python Documentation�h��$https://docs.python.org/2/index.html�uh)h�hj�  ubh�)��}�(h�'
<https://docs.python.org/2/index.html>�h]�h}�(h]��python-documentation�ah!]�h#]��python documentation�ah%]�h']��refuri�j  uh)h�h�Khj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK\hj9  hhubeh}�(h]��%setting-the-environment-variable-path�ah!]�h#]��%setting the environment variable path�ah%]�h']�uh)h	hhhhhh*hK/ubh
)��}�(hhh]�(h)��}�(h�NAOqi for Python�h]�h�NAOqi for Python�����}�(hj3  hj1  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj.  hhhh*hK`ubh,)��}�(h�FOnce python is installed, you need to use Aldebaran's NAOqi framework.�h]�h�HOnce python is installed, you need to use Aldebaran’s NAOqi framework.�����}�(hjA  hj?  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKahj.  hhubh,)��}�(h��Visit `SoftBank Robotics Community <https://community.ald.softbankrobotics.com/en/resources/software/language/en-gb>`_
and download **[FORMER NAO VERSIONS] - Python NAOqi SDK**. I suggest doing the setup download
as you will get the exe file.�h]�(h�Visit �����}�(h�Visit �hjM  hhhNhNubh�)��}�(h�p`SoftBank Robotics Community <https://community.ald.softbankrobotics.com/en/resources/software/language/en-gb>`_�h]�h�SoftBank Robotics Community�����}�(h�SoftBank Robotics Community�hjV  ubah}�(h]�h!]�h#]�h%]�h']��name��SoftBank Robotics Community�h��Ohttps://community.ald.softbankrobotics.com/en/resources/software/language/en-gb�uh)h�hjM  ubh�)��}�(h�R <https://community.ald.softbankrobotics.com/en/resources/software/language/en-gb>�h]�h}�(h]��softbank-robotics-community�ah!]�h#]��softbank robotics community�ah%]�h']��refuri�jg  uh)h�h�KhjM  ubh�
and download �����}�(h�
and download �hjM  hhhNhNubh�)��}�(h�,**[FORMER NAO VERSIONS] - Python NAOqi SDK**�h]�h�([FORMER NAO VERSIONS] - Python NAOqi SDK�����}�(hhhjz  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hjM  ubh�B. I suggest doing the setup download
as you will get the exe file.�����}�(h�B. I suggest doing the setup download
as you will get the exe file.�hjM  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKchj.  hhubh,)��}�(h�@Once you begin your installation, you will arrive at this point:�h]�h�@Once you begin your installation, you will arrive at this point:�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKghj.  hhubh �image���)��}�(h�V.. image:: ./imgs/pynaoqi.png
    :width: 500px
    :align: center
    :height: 350px
�h]�h}�(h]�h!]�h#]�h%]�h']��width��500px��align��center��height��350px��uri��./imgs/pynaoqi.png��
candidates�}��*�j�  suh)j�  hj.  hhhh*hNubh,)��}�(h��If you have multiple versions of python installed, make sure you have
selected the **correct version of python (python 2.7)** otherwise you
will get import issues.�h]�(h�SIf you have multiple versions of python installed, make sure you have
selected the �����}�(h�SIf you have multiple versions of python installed, make sure you have
selected the �hj�  hhhNhNubh�)��}�(h�***correct version of python (python 2.7)**�h]�h�&correct version of python (python 2.7)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hj�  ubh�& otherwise you
will get import issues.�����}�(h�& otherwise you
will get import issues.�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKnhj.  hhubh,)��}�(h�7After the installation is done, it will allow you to do�h]�h�7After the installation is done, it will allow you to do�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKrhj.  hhubj  )��}�(h�import naoqi
import qi�h]�h�import naoqi
import qi�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']��force���highlight_args�}�hKhLj/  �console�uh)j  hh*hKthj.  hhubh,)��}�(h�in your python files.�h]�h�in your python files.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKyhj.  hhubh,)��}�(h�KIf these lines run without any issues, we are ready for our first tutorial.�h]�h�KIf these lines run without any issues, we are ready for our first tutorial.�����}�(hj
  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK{hj.  hhubeh}�(h]��naoqi-for-python�ah!]�h#]��naoqi for python�ah%]�h']�uh)h	hhhhhh*hK`ubh
)��}�(hhh]�(h)��}�(h�RoboWalknTalk�h]�h�RoboWalknTalk�����}�(hj#  hj!  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj  hhhh*hKubh,)��}�(h�FThis tutorial follows parts of examples from the documentation of NAO.�h]�h�FThis tutorial follows parts of examples from the documentation of NAO.�����}�(hj1  hj/  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubh,)��}�(h�iFor more information, see: `NAOqi's Developer Guide <http://doc.aldebaran.com/2-8/index_dev_guide.html>`_�h]�(h�For more information, see: �����}�(h�For more information, see: �hj=  hhhNhNubh�)��}�(h�N`NAOqi's Developer Guide <http://doc.aldebaran.com/2-8/index_dev_guide.html>`_�h]�h�NAOqi’s Developer Guide�����}�(h�NAOqi's Developer Guide�hjF  ubah}�(h]�h!]�h#]�h%]�h']��name��NAOqi's Developer Guide�h��1http://doc.aldebaran.com/2-8/index_dev_guide.html�uh)h�hj=  ubh�)��}�(h�4 <http://doc.aldebaran.com/2-8/index_dev_guide.html>�h]�h}�(h]��naoqi-s-developer-guide�ah!]�h#]��naoqi's developer guide�ah%]�h']��refuri�jW  uh)h�h�Khj=  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubh,)��}�(h�?Open your favorite Python IDE. Run the following lines of code:�h]�h�?Open your favorite Python IDE. Run the following lines of code:�����}�(hjm  hjk  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubj  )��}�(h��from naoqi import ALProxy
import qi
roboIP = "NAO_IP"   # Example: roboIP = "172.18.72.172"
roboPort = 9559     # This is by default
motion = ALProxy("ALMotion", roboIP, roboPort)
tts = ALProxy("ALTextToSpeech", roboIP, roboPort)�h]�h��from naoqi import ALProxy
import qi
roboIP = "NAO_IP"   # Example: roboIP = "172.18.72.172"
roboPort = 9559     # This is by default
motion = ALProxy("ALMotion", roboIP, roboPort)
tts = ALProxy("ALTextToSpeech", roboIP, roboPort)�����}�(hhhjy  ubah}�(h]�h!]�h#]�h%]�h']�hKhL�linenos���force��j/  �python��highlight_args�}�uh)j  hh*hK�hj  hhubh,)��}�(hX
  Lines 1-2 are importing the NAOqi for Python framework.
For line 3 you need to set your NAO ip address. You can obtain the IP of NAO by simply pressing the button on his chest.
For line 4, we are setting up the default port that NAO uses to communicate with your PC.�h]�hX
  Lines 1-2 are importing the NAOqi for Python framework.
For line 3 you need to set your NAO ip address. You can obtain the IP of NAO by simply pressing the button on his chest.
For line 4, we are setting up the default port that NAO uses to communicate with your PC.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubh,)��}�(hXf  Line 5 and 6, the `ALProxy <http://doc.aldebaran.com/2-8/dev/python/making_nao_speak.html?highlight=alproxy#ALProxy>`_
object lets you create a proxy to a module. This lets you use
`ALMotion  <http://doc.aldebaran.com/2-8/naoqi/motion/almotion-api.html#almotion-api>`_ and
`ALTextToSpeech <http://doc.aldebaran.com/2-4/naoqi/audio/altexttospeech-api.html>`_.�h]�(h�Line 5 and 6, the �����}�(h�Line 5 and 6, the �hj�  hhhNhNubh�)��}�(h�d`ALProxy <http://doc.aldebaran.com/2-8/dev/python/making_nao_speak.html?highlight=alproxy#ALProxy>`_�h]�h�ALProxy�����}�(h�ALProxy�hj�  ubah}�(h]�h!]�h#]�h%]�h']��name�j�  h��Whttp://doc.aldebaran.com/2-8/dev/python/making_nao_speak.html?highlight=alproxy#ALProxy�uh)h�hj�  ubh�)��}�(h�Z <http://doc.aldebaran.com/2-8/dev/python/making_nao_speak.html?highlight=alproxy#ALProxy>�h]�h}�(h]��alproxy�ah!]�h#]��alproxy�ah%]�h']��refuri�j�  uh)h�h�Khj�  ubh�?
object lets you create a proxy to a module. This lets you use
�����}�(h�?
object lets you create a proxy to a module. This lets you use
�hj�  hhhNhNubh�)��}�(h�W`ALMotion  <http://doc.aldebaran.com/2-8/naoqi/motion/almotion-api.html#almotion-api>`_�h]�h�ALMotion�����}�(h�ALMotion�hj�  ubah}�(h]�h!]�h#]�h%]�h']��name�j�  h��Hhttp://doc.aldebaran.com/2-8/naoqi/motion/almotion-api.html#almotion-api�uh)h�hj�  ubh�)��}�(h�L  <http://doc.aldebaran.com/2-8/naoqi/motion/almotion-api.html#almotion-api>�h]�h}�(h]��almotion�ah!]�h#]��almotion�ah%]�h']��refuri�j�  uh)h�h�Khj�  ubh� and
�����}�(h� and
�hj�  hhhNhNubh�)��}�(h�T`ALTextToSpeech <http://doc.aldebaran.com/2-4/naoqi/audio/altexttospeech-api.html>`_�h]�h�ALTextToSpeech�����}�(h�ALTextToSpeech�hj�  ubah}�(h]�h!]�h#]�h%]�h']��name�j�  h��@http://doc.aldebaran.com/2-4/naoqi/audio/altexttospeech-api.html�uh)h�hj�  ubh�)��}�(h�C <http://doc.aldebaran.com/2-4/naoqi/audio/altexttospeech-api.html>�h]�h}�(h]��altexttospeech�ah!]�h#]��altexttospeech�ah%]�h']��refuri�j�  uh)h�h�Khj�  ubh�.�����}�(h�.�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubh,)��}�(h�The next part of our script is:�h]�h�The next part of our script is:�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubj  )��}�(hX{  # Stiffness value has to be greater than 0
motion.moveInit()
motion.setStiffnesses("Body", 1.0)
motion.post.moveTo(0.5,0,0)
tts.say("I can walk!")
tts.say("I can do multiple thigns at the same time as well.")
tts.say("It is only a matter of time before I take over the world!")
motion.moveTo(0,0, 1.5709*2)
motion.post.moveTo(0.5,0,0)
tts.say("I can even turn around on my own!")�h]�hX{  # Stiffness value has to be greater than 0
motion.moveInit()
motion.setStiffnesses("Body", 1.0)
motion.post.moveTo(0.5,0,0)
tts.say("I can walk!")
tts.say("I can do multiple thigns at the same time as well.")
tts.say("It is only a matter of time before I take over the world!")
motion.moveTo(0,0, 1.5709*2)
motion.post.moveTo(0.5,0,0)
tts.say("I can even turn around on my own!")�����}�(hhhj   ubah}�(h]�h!]�h#]�h%]�h']�hKhLj�  �j�  �j/  �python�j�  }��linenostart�Ksuh)j  hh*hK�hj  hhubh,)��}�(h�|First off, we are required to have NAO in the correct position to walk. We use the function moveInit() in line 8 to do this.�h]�h�|First off, we are required to have NAO in the correct position to walk. We use the function moveInit() in line 8 to do this.�����}�(hj3  hj1  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubh,)��}�(h��Second, the robot will not walk unless the stiffness levels are set. That is, any value greater than 0 will work.
We do this in line 9 by using the function setStiffness("Body", 1.0) where the value 1.0 is the stiffness level.�h]�h��Second, the robot will not walk unless the stiffness levels are set. That is, any value greater than 0 will work.
We do this in line 9 by using the function setStiffness(“Body”, 1.0) where the value 1.0 is the stiffness level.�����}�(hjA  hj?  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubh,)��}�(hX�  In line 10, we make use of the post method. This method will allow NAO to speak and walk at the same time through synchronous
programming. For more information, visit `Blocking and Non-Blocking Calls <http://doc.aldebaran.com/2-8/dev/naoqi/index.html#naoqi-blocking-non-blocking>`_.
We also use the `moveTo() <http://doc.aldebaran.com/1-14/naoqi/motion/control-walk-api.html#ALMotionProxy::moveTo__floatCR.floatCR.floatCR>`_
method to make NAO walk.�h]�(h��In line 10, we make use of the post method. This method will allow NAO to speak and walk at the same time through synchronous
programming. For more information, visit �����}�(h��In line 10, we make use of the post method. This method will allow NAO to speak and walk at the same time through synchronous
programming. For more information, visit �hjM  hhhNhNubh�)��}�(h�r`Blocking and Non-Blocking Calls <http://doc.aldebaran.com/2-8/dev/naoqi/index.html#naoqi-blocking-non-blocking>`_�h]�h�Blocking and Non-Blocking Calls�����}�(h�Blocking and Non-Blocking Calls�hjV  ubah}�(h]�h!]�h#]�h%]�h']��name��Blocking and Non-Blocking Calls�h��Mhttp://doc.aldebaran.com/2-8/dev/naoqi/index.html#naoqi-blocking-non-blocking�uh)h�hjM  ubh�)��}�(h�P <http://doc.aldebaran.com/2-8/dev/naoqi/index.html#naoqi-blocking-non-blocking>�h]�h}�(h]��blocking-and-non-blocking-calls�ah!]�h#]��blocking and non-blocking calls�ah%]�h']��refuri�jg  uh)h�h�KhjM  ubh�.
We also use the �����}�(h�.
We also use the �hjM  hhhNhNubh�)��}�(h�}`moveTo() <http://doc.aldebaran.com/1-14/naoqi/motion/control-walk-api.html#ALMotionProxy::moveTo__floatCR.floatCR.floatCR>`_�h]�h�moveTo()�����}�(h�moveTo()�hjz  ubah}�(h]�h!]�h#]�h%]�h']��name�j�  h��ohttp://doc.aldebaran.com/1-14/naoqi/motion/control-walk-api.html#ALMotionProxy::moveTo__floatCR.floatCR.floatCR�uh)h�hjM  ubh�)��}�(h�r <http://doc.aldebaran.com/1-14/naoqi/motion/control-walk-api.html#ALMotionProxy::moveTo__floatCR.floatCR.floatCR>�h]�h}�(h]��moveto�ah!]�h#]��moveto()�ah%]�h']��refuri�j�  uh)h�h�KhjM  ubh�
method to make NAO walk.�����}�(h�
method to make NAO walk.�hjM  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubh,)��}�(h��In short, moveTo(x, y, z) means x and y are coordinate planes where the front of NAO is the positive
x axis and the right arm direction is the positive y axis. The z variable tells NAO how much to rotate.�h]�h��In short, moveTo(x, y, z) means x and y are coordinate planes where the front of NAO is the positive
x axis and the right arm direction is the positive y axis. The z variable tells NAO how much to rotate.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubh,)��}�(h�nAs NAO continues to walk, it will continue to run lines 11-13. The say(string) function tells NAO what to say.�h]�h�nAs NAO continues to walk, it will continue to run lines 11-13. The say(string) function tells NAO what to say.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubh,)��}�(h�Once NAO finishes walking, it will execute line 14. The value 1.5709*2 is simply pi, telling NAO to rotate pi, or 180, degrees.�h]�h�Once NAO finishes walking, it will execute line 14. The value 1.5709*2 is simply pi, telling NAO to rotate pi, or 180, degrees.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubh,)��}�(h�RLine 15 tells NAO to move again 0.5 meters and as NAO walks, line 16 will execute.�h]�h�RLine 15 tells NAO to move again 0.5 meters and as NAO walks, line 16 will execute.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubh,)��}�(h��For other variations such as having NAO talk first then walk or vice versa, visit
this `site <http://doc.aldebaran.com/2-8/dev/python/making_nao_move.html#making-nao-move-and-speak-at-the-same-time>`_.�h]�(h�WFor other variations such as having NAO talk first then walk or vice versa, visit
this �����}�(h�WFor other variations such as having NAO talk first then walk or vice versa, visit
this �hj�  hhhNhNubh�)��}�(h�q`site <http://doc.aldebaran.com/2-8/dev/python/making_nao_move.html#making-nao-move-and-speak-at-the-same-time>`_�h]�h�site�����}�(h�site�hj�  ubah}�(h]�h!]�h#]�h%]�h']��name�j�  h��ghttp://doc.aldebaran.com/2-8/dev/python/making_nao_move.html#making-nao-move-and-speak-at-the-same-time�uh)h�hj�  ubh�)��}�(h�j <http://doc.aldebaran.com/2-8/dev/python/making_nao_move.html#making-nao-move-and-speak-at-the-same-time>�h]�h}�(h]��site�ah!]�h#]��site�ah%]�h']��refuri�j�  uh)h�h�Khj�  ubh�.�����}�(hj  hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubh,)��}�(h��You have now completed your first script! I suggest visiting the NAOqi framework website to learn more and create other interesting
scripts.�h]�h��You have now completed your first script! I suggest visiting the NAOqi framework website to learn more and create other interesting
scripts.�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hj  hhubeh}�(h]��robowalkntalk�ah!]�h#]��robowalkntalk�ah%]�h']�uh)h	hhhhhh*hKubeh}�(h]��python�ah!]�h#]��python�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jM  �error_encoding��cp1252��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j'  j$  j6  j3  h�h�h�h�j+  j(  j  j  j  j  jq  jn  j  j  ja  j^  j�  j�  j�  j�  j  j   jq  jn  j�  j�  j�  j�  u�	nametypes�}�(j'  Nj6  Nh��h��j+  Nj  �j  Njq  �j  Nja  �j�  �j�  �j  �jq  �j�  �j�  �uh}�(j$  hj3  hsh�h�h�h�j(  j9  j  j  j  j.  jn  jh  j  j  j^  jX  j�  j�  j�  j�  j   j�  jn  jh  j�  j�  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]�(h �system_message���)��}�(hhh]�h,)��}�(h�Atoctree contains reference to nonexisting document 'Installation'�h]�h�Etoctree contains reference to nonexisting document ‘Installation’�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type��WARNING��line�K�source�h*uh)j�  hhhhhh*hNubj�  )��}�(hhh]�h,)��}�(h�Ztoctree contains reference to nonexisting document 'Setting the Environment Variable path'�h]�h�^toctree contains reference to nonexisting document ‘Setting the Environment Variable path’�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j�  �line�K�source�h*uh)j�  hhhhhh*hNubj�  )��}�(hhh]�h,)��}�(h�Etoctree contains reference to nonexisting document 'NAOqi for Python'�h]�h�Itoctree contains reference to nonexisting document ‘NAOqi for Python’�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j�  �line�K�source�h*uh)j�  hhhhhh*hNubj�  )��}�(hhh]�h,)��}�(h�Btoctree contains reference to nonexisting document 'RoboWalknTalk'�h]�h�Ftoctree contains reference to nonexisting document ‘RoboWalknTalk’�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j�  �line�K�source�h*uh)j�  hhhhhh*hNube�transform_messages�]��transformer�N�
decoration�Nhhub.