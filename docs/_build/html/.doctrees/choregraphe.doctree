��V      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Choregraphe�h]�h �Text����Choregraphe�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�Xc:\Users\Chick3n\Desktop\ECU Spring 2020\NAO Robot Project\nao-wnlp\docs\choregraphe.rst�hKubh �	paragraph���)��}�(h�EChoregraphe is a multi-platform desktop application, allowing you to:�h]�h�EChoregraphe is a multi-platform desktop application, allowing you to:�����}�(hh/hh-hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�)Create animations, behaviors and dialogs,�h]�h,)��}�(hhDh]�h�)Create animations, behaviors and dialogs,�����}�(hhDhhFubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhBubah}�(h]�h!]�h#]�h%]�h']�uh)h@hh=hhhh*hNubhA)��}�(h�:Test them on a simulated robot, or directly on a real one,�h]�h,)��}�(hh[h]�h�:Test them on a simulated robot, or directly on a real one,�����}�(hh[hh]ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhYubah}�(h]�h!]�h#]�h%]�h']�uh)h@hh=hhhh*hNubhA)��}�(h�Monitor and control you robot,�h]�h,)��}�(hhrh]�h�Monitor and control you robot,�����}�(hhrhhtubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhpubah}�(h]�h!]�h#]�h%]�h']�uh)h@hh=hhhh*hNubhA)��}�(h�8Enrich Choregraphe behaviors with your own Python code.
�h]�h,)��}�(h�7Enrich Choregraphe behaviors with your own Python code.�h]�h�7Enrich Choregraphe behaviors with your own Python code.�����}�(hh�hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK	hh�ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hh=hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']��bullet��*�uh)h;hh*hKhhhhubh,)��}�(h�nAs always, visit the `official documentation <http://doc.aldebaran.com/2-8/index.html>`_
for more information.�h]�(h�As always, visit the �����}�(h�As always, visit the �hh�hhhNhNubh �	reference���)��}�(h�C`official documentation <http://doc.aldebaran.com/2-8/index.html>`_�h]�h�official documentation�����}�(h�official documentation�hh�ubah}�(h]�h!]�h#]�h%]�h']��name��official documentation��refuri��'http://doc.aldebaran.com/2-8/index.html�uh)h�hh�ubh �target���)��}�(h�* <http://doc.aldebaran.com/2-8/index.html>�h]�h}�(h]��official-documentation�ah!]�h#]��official documentation�ah%]�h']��refuri�h�uh)hŌ
referenced�Khh�ubh�
for more information.�����}�(h�
for more information.�hh�hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh �compound���)��}�(hhh]��sphinx.addnodes��toctree���)��}�(hhh]�h}�(h]�h!]�h#]�h%]�h']�h�choregraphe��entries�]��includefiles�]��maxdepth�K�caption�N�glob���hidden���includehidden���numbered�K �
titlesonly���
rawentries�]�uh)h�hh*hKhh�ubah}�(h]�h!]��toctree-wrapper�ah#]�h%]�h']�uh)h�hhhhhh*hNubh
)��}�(hhh]�(h)��}�(h�Installation�h]�h�Installation�����}�(hj  hj	  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj  hhhh*hKubh,)��}�(h��For installation, follow Aldebaran's official installation guide
at:
`Aldebaran's installation guide
<http://doc.aldebaran.com/2-1/getting_started/installing.html#desktop-installation>`_�h]�(h�GFor installation, follow Aldebaran’s official installation guide
at:
�����}�(h�EFor installation, follow Aldebaran's official installation guide
at:
�hj  hhhNhNubh�)��}�(h�u`Aldebaran's installation guide
<http://doc.aldebaran.com/2-1/getting_started/installing.html#desktop-installation>`_�h]�h� Aldebaran’s installation guide�����}�(h�Aldebaran's installation guide�hj   ubah}�(h]�h!]�h#]�h%]�h']��name��Aldebaran's installation guide�hÌQhttp://doc.aldebaran.com/2-1/getting_started/installing.html#desktop-installation�uh)h�hj  ubh�)��}�(h�T
<http://doc.aldebaran.com/2-1/getting_started/installing.html#desktop-installation>�h]�h}�(h]��aldebaran-s-installation-guide�ah!]�h#]��aldebaran's installation guide�ah%]�h']��refuri�j1  uh)h�h�Khj  ubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhj  hhubeh}�(h]��installation�ah!]�h#]��installation�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Choregraphe: Quick Navigation�h]�h�Choregraphe: Quick Navigation�����}�(hjR  hjP  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhjM  hhhh*hKubh �figure���)��}�(hhh]�(h �image���)��}�(h�B.. figure:: ./imgs/chorgui.jpg
   :scale: 75%

   Choregraphe GUI
�h]�h}�(h]�h!]�h#]�h%]�h']��scale�KK�uri��./imgs/chorgui.jpg��
candidates�}�h�jq  suh)jc  hj`  hh*hK&ubh h���)��}�(h�Choregraphe GUI�h]�h�Choregraphe GUI�����}�(hjw  hju  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh*hK&hj`  ubeh}�(h]��id1�ah!]�h#]�h%]�h']��align��default�uh)j^  hK&hjM  hhhh*ubh �enumerated_list���)��}�(hhh]�(hA)��}�(h�<Project files: These are the files for your current project.�h]�h,)��}�(hj�  h]�h�<Project files: These are the files for your current project.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK(hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hj�  hhhh*hNubhA)��}�(h�RBox libraries: This contains behaviors that you will use throughout your projects.�h]�h,)��}�(hj�  h]�h�RBox libraries: This contains behaviors that you will use throughout your projects.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK)hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hj�  hhhh*hNubhA)��}�(h�?Essentially your canvas. You drag and drop your behaviors here.�h]�h,)��}�(hj�  h]�h�?Essentially your canvas. You drag and drop your behaviors here.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK*hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hj�  hhhh*hNubhA)��}�(h�)Log viewer: A log for debugging purposes.�h]�h,)��}�(hj�  h]�h�)Log viewer: A log for debugging purposes.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK+hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hj�  hhhh*hNubhA)��}�(h�KRobot view: Essentially helps with what NAO is seeing and its surroundings.�h]�h,)��}�(hj�  h]�h�KRobot view: Essentially helps with what NAO is seeing and its surroundings.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK,hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hj�  hhhh*hNubhA)��}�(h�CInspector: Gives information about your behaviors and other items.
�h]�h,)��}�(h�BInspector: Gives information about your behaviors and other items.�h]�h�BInspector: Gives information about your behaviors and other items.�����}�(hj
  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK-hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h@hj�  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']��enumtype��arabic��prefix�h�suffix��.�uh)j�  hjM  hhhh*hK(ubeh}�(h]��choregraphe-quick-navigation�ah!]�h#]��choregraphe: quick navigation�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Creating A Behavior�h]�h�Creating A Behavior�����}�(hj4  hj2  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj/  hhhh*hK0ubh,)��}�(hX  Launch Choregraphe and create a new project through the File menu. By default,
you should have the Box libraries window on the bottom left. Click the search icon
(Show Filter button) and type in "say" in the text box. Drag and drop the
behavior "Say" into your canvas.�h]�hX  Launch Choregraphe and create a new project through the File menu. By default,
you should have the Box libraries window on the bottom left. Click the search icon
(Show Filter button) and type in “say” in the text box. Drag and drop the
behavior “Say” into your canvas.�����}�(hjB  hj@  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK2hj/  hhubj_  )��}�(hhh]�(jd  )��}�(h�A.. figure:: ./imgs/behcon.jpg
   :scale: 75%

   onStart buttons
�h]�h}�(h]�h!]�h#]�h%]�h']��scale�KK�uri��./imgs/behcon.jpg�jr  }�h�j]  suh)jc  hjN  hh*hK;ubjt  )��}�(h�onStart buttons�h]�h�onStart buttons�����}�(hja  hj_  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh*hK;hjN  ubeh}�(h]��id2�ah!]�h#]�h%]�h']�j�  j�  uh)j^  hK;hj/  hhhh*ubh,)��}�(h��Next, click the onStart button, hold, and drag over to the onStart button of the Say behavior.
Likewise, connect the onStopped button on the Say behavior to the right-most onStopped button.�h]�h��Next, click the onStart button, hold, and drag over to the onStart button of the Say behavior.
Likewise, connect the onStopped button on the Say behavior to the right-most onStopped button.�����}�(hjv  hjt  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK=hj/  hhubj_  )��}�(hhh]�(jd  )��}�(h�D.. figure:: ./imgs/behcon1.jpg
   :scale: 75%

   Final connections
�h]�h}�(h]�h!]�h#]�h%]�h']��scale�KK�uri��./imgs/behcon1.jpg�jr  }�h�j�  suh)jc  hj�  hh*hKCubjt  )��}�(h�Final connections�h]�h�Final connections�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh*hKChj�  ubeh}�(h]��id3�ah!]�h#]�h%]�h']�j�  j�  uh)j^  hKChj/  hhhh*ubh,)��}�(h�4The figure above demonstrates the final connections.�h]�h�4The figure above demonstrates the final connections.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKEhj/  hhubh,)��}�(hX  Next, click the wrench in the Say behavior. This will open the Set Parameter
box. You can play around with the voice shaping and speed parameters to your liking.
In the Text box, NAO will say whatever is typed here. To play, simply hit the
Play button (f5) found above the canvas.�h]�hX  Next, click the wrench in the Say behavior. This will open the Set Parameter
box. You can play around with the voice shaping and speed parameters to your liking.
In the Text box, NAO will say whatever is typed here. To play, simply hit the
Play button (f5) found above the canvas.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKGhj/  hhubh,)��}�(h�wI suggest to explore the folders in the Box library window to get a sense
of the different behaviors you can work with.�h]�h�wI suggest to explore the folders in the Box library window to get a sense
of the different behaviors you can work with.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKLhj/  hhubeh}�(h]��creating-a-behavior�ah!]�h#]��creating a behavior�ah%]�h']�uh)h	hhhhhh*hK0ubh
)��}�(hhh]�(h)��}�(h�Connect Different Behaviors�h]�h�Connect Different Behaviors�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hKPubh,)��}�(h�FYou can connect different behaviors to create a more complex behavior.�h]�h�FYou can connect different behaviors to create a more complex behavior.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKRhj�  hhubh,)��}�(h�?With enough playing around, you can create something like this:�h]�h�?With enough playing around, you can create something like this:�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKThj�  hhubj_  )��}�(hhh]�(jd  )��}�(h��.. figure:: ./imgs/rechuman.jpg
   :scale: 75%

   A simple behavior which demonstrates the usage of conditional Switch Cases, speech
   recognition, say, and face detection.
�h]�h}�(h]�h!]�h#]�h%]�h']��scale�KK�uri��./imgs/rechuman.jpg�jr  }�h�j  suh)jc  hj  hh*hKYubjt  )��}�(h�xA simple behavior which demonstrates the usage of conditional Switch Cases, speech
recognition, say, and face detection.�h]�h�xA simple behavior which demonstrates the usage of conditional Switch Cases, speech
recognition, say, and face detection.�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh*hKYhj  ubeh}�(h]��id4�ah!]�h#]�h%]�h']�j�  j�  uh)j^  hKYhj�  hhhh*ubh,)��}�(hX/  In this behavior, it uses NAO's speech recognition behavior to catch a phrase.
Speech recognition loops until you say any phrase. In this case, the phrase
"Am I a human?" or "Am I a dog?" will activate a switch case statement that
responds accordingly. If NAO can't recognize
what you said, it will run the bottom branch of the speech recognition
behavior, the onNothing button. Otherwise once the phrase is processed, NAO responds accordingly.
You can even use the Play Sound behavior to play audio files. In the project
above, NAO plays a dog barking sound.�h]�hX;  In this behavior, it uses NAO’s speech recognition behavior to catch a phrase.
Speech recognition loops until you say any phrase. In this case, the phrase
“Am I a human?” or “Am I a dog?” will activate a switch case statement that
responds accordingly. If NAO can’t recognize
what you said, it will run the bottom branch of the speech recognition
behavior, the onNothing button. Otherwise once the phrase is processed, NAO responds accordingly.
You can even use the Play Sound behavior to play audio files. In the project
above, NAO plays a dog barking sound.�����}�(hj/  hj-  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK\hj�  hhubh,)��}�(h��You can download this behavior from my
`GitLab <https://gitlab.com/LeoEspinoza/nao-wnlp/-/tree/master/choregraphe-func/recognize%20humans>`_.�h]�(h�'You can download this behavior from my
�����}�(h�'You can download this behavior from my
�hj;  hhhNhNubh�)��}�(h�e`GitLab <https://gitlab.com/LeoEspinoza/nao-wnlp/-/tree/master/choregraphe-func/recognize%20humans>`_�h]�h�GitLab�����}�(h�GitLab�hjD  ubah}�(h]�h!]�h#]�h%]�h']��name�jL  hÌYhttps://gitlab.com/LeoEspinoza/nao-wnlp/-/tree/master/choregraphe-func/recognize%20humans�uh)h�hj;  ubh�)��}�(h�\ <https://gitlab.com/LeoEspinoza/nao-wnlp/-/tree/master/choregraphe-func/recognize%20humans>�h]�h}�(h]��gitlab�ah!]�h#]��gitlab�ah%]�h']��refuri�jT  uh)h�h�Khj;  ubh�.�����}�(hj&  hj;  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKehj�  hhubeh}�(h]��connect-different-behaviors�ah!]�h#]��connect different behaviors�ah%]�h']�uh)h	hhhhhh*hKPubh
)��}�(hhh]�(h)��}�(h�"Transfering a Behavior to your NAO�h]�h�"Transfering a Behavior to your NAO�����}�(hjy  hjw  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhjt  hhhh*hKiubh,)��}�(h�yOnce you are finished with a behavior and testing it out, you can upload it
into NAO's memory so it can play it any time.�h]�h�{Once you are finished with a behavior and testing it out, you can upload it
into NAO’s memory so it can play it any time.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKkhjt  hhubj_  )��}�(hhh]�(jd  )��}�(h�P.. figure:: ./imgs/uploadBeh.jpg
   :scale: 75%

   Package and install button

�h]�h}�(h]�h!]�h#]�h%]�h']��scale�KK�uri��./imgs/uploadBeh.jpg�jr  }�h�j�  suh)jc  hj�  hh*hKrubjt  )��}�(h�Package and install button�h]�h�Package and install button�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh*hKrhj�  ubeh}�(h]��id5�ah!]�h#]�h%]�h']�j�  j�  uh)j^  hKrhjt  hhhh*ubh,)��}�(h��To upload it, click on the Robot application tab in the inspector window.
Next click the Package and install button as shown above. Once this is done,
you have to configure the trigger words to activate the behavior.�h]�h��To upload it, click on the Robot application tab in the inspector window.
Next click the Package and install button as shown above. Once this is done,
you have to configure the trigger words to activate the behavior.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKuhjt  hhubj_  )��}�(hhh]�(jd  )��}�(h�X.. figure:: ./imgs/projProp.jpg
   :scale: 75%

   Click where the red arrows points to
�h]�h}�(h]�h!]�h#]�h%]�h']��scale�KK�uri��./imgs/projProp.jpg�jr  }�h�j�  suh)jc  hj�  hh*hK|ubjt  )��}�(h�$Click where the red arrows points to�h]�h�$Click where the red arrows points to�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh*hK|hj�  ubeh}�(h]��id6�ah!]�h#]�h%]�h']�j�  j�  uh)j^  hK|hjt  hhhh*ubh,)��}�(h��To do so, click on the Properties button located in the Project files window.
Next click the behavior_1 (default name) property to access more settings.�h]�h��To do so, click on the Properties button located in the Project files window.
Next click the behavior_1 (default name) property to access more settings.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK~hjt  hhubj_  )��}�(hhh]�(jd  )��}�(h�v.. figure:: ./imgs/projProp1.jpg
   :scale: 100%

   You enter your trigger word or sentence in the far right option

�h]�h}�(h]�h!]�h#]�h%]�h']��scale�Kd�uri��./imgs/projProp1.jpg�jr  }�h�j
  suh)jc  hj�  hh*hK�ubjt  )��}�(h�?You enter your trigger word or sentence in the far right option�h]�h�?You enter your trigger word or sentence in the far right option�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h�hh*hK�hj�  ubeh}�(h]��id7�ah!]�h#]�h%]�h']�j�  j�  uh)j^  hK�hjt  hhhh*ubh,)��}�(hX'  On the far right we have the Trigger sentences text box. Enter in a word
such as "robo" and hit Add. It will automatically populate the middle
Trigger sentences box. Once finished, you can disconnect your NAO robot
Now simply say your trigger word and wait for NAO to go into its
listening mode.�h]�hX+  On the far right we have the Trigger sentences text box. Enter in a word
such as “robo” and hit Add. It will automatically populate the middle
Trigger sentences box. Once finished, you can disconnect your NAO robot
Now simply say your trigger word and wait for NAO to go into its
listening mode.�����}�(hj#  hj!  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hjt  hhubh,)��}�(h�If everything worked out good, NAO should do whatever you tell it to do.
For more information, visit the offical documentation.�h]�h�If everything worked out good, NAO should do whatever you tell it to do.
For more information, visit the offical documentation.�����}�(hj1  hj/  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK�hjt  hhubeh}�(h]��"transfering-a-behavior-to-your-nao�ah!]�h#]��"transfering a behavior to your nao�ah%]�h']�uh)h	hhhhhh*hKiubeh}�(h]��choregraphe�ah!]�h#]��choregraphe�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jp  �error_encoding��cp1252��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(jJ  jG  h�h�jJ  jG  j;  j8  j,  j)  j�  j�  jq  jn  j^  j[  jB  j?  u�	nametypes�}�(jJ  NhЈjJ  Nj;  �j,  Nj�  Njq  Nj^  �jB  Nuh}�(jG  hh�h�jG  j  j8  j2  j)  jM  j�  j/  jn  j�  j[  jU  j?  jt  j�  j`  jo  jN  j�  j�  j(  j  j�  j�  j�  j�  j  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�j~  Ks��R��parse_messages�]�(h �system_message���)��}�(hhh]�h,)��}�(h�Atoctree contains reference to nonexisting document 'Installation'�h]�h�Etoctree contains reference to nonexisting document ‘Installation’�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type��WARNING��line�K�source�h*uh)j�  hhhhhh*hNubj�  )��}�(hhh]�h,)��}�(h�Rtoctree contains reference to nonexisting document 'Choregraphe: Quick Navigation'�h]�h�Vtoctree contains reference to nonexisting document ‘Choregraphe: Quick Navigation’�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj�  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j�  �line�K�source�h*uh)j�  hhhhhh*hNubj�  )��}�(hhh]�h,)��}�(h�Htoctree contains reference to nonexisting document 'Creating A Behavior'�h]�h�Ltoctree contains reference to nonexisting document ‘Creating A Behavior’�����}�(hhhj	  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j�  �line�K�source�h*uh)j�  hhhhhh*hNubj�  )��}�(hhh]�h,)��}�(h�Ptoctree contains reference to nonexisting document 'Connect Different Behaviors'�h]�h�Ttoctree contains reference to nonexisting document ‘Connect Different Behaviors’�����}�(hhhj$  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj!  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j�  �line�K�source�h*uh)j�  hhhhhh*hNubj�  )��}�(hhh]�h,)��}�(h�Wtoctree contains reference to nonexisting document 'Transfering a Behavior to your NAO'�h]�h�[toctree contains reference to nonexisting document ‘Transfering a Behavior to your NAO’�����}�(hhhj?  ubah}�(h]�h!]�h#]�h%]�h']�uh)h+hj<  ubah}�(h]�h!]�h#]�h%]�h']��level�K�type�j�  �line�K�source�h*uh)j�  hhhhhh*hNube�transform_messages�]��transformer�N�
decoration�Nhhub.