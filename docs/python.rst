Python
======================

NAO robot works with Python 2.7. This guide will walk you through the
installation process of Python.

.. 
    The directive toctree lets us combine multiple pages into a 
    cohesive hierarchy. It outputs a Table of Contents in the
    page where it occurs. The maxdepth option/argument tells
    Sphinx to include 2 levels of headers. It lets Sphinx
    know that the other pages are sub-pages of the current page,
    creating a "tree" structure of the pages:
    Python
    ├── Installation
    ├── Setting the Environment Variable path
    For more information, visit,
    https://www.ericholscher.com/blog/2016/jul/1/sphinx-and-rtd-for-writers/


.. toctree::
   :maxdepth: 2

   Installation
   Setting the Environment Variable path
   NAOqi for Python
   RoboWalknTalk

Installation
-----------------------

You can download the python programming language from 
`Python's <https://www.python.org/downloads/>`_ official website. For this
project, Python version 2.7.17 32-bit was used.

For a quick download: `Python 2.7.17 <https://www.python.org/ftp/python/2.7.17/python-2.7.17.msi>`_

.. warning:: **Note that a 32-bit version of Python is REQUIRED.**

I suggest installing Python in the default path:

.. code:: console
    
    C:\Python27.

Setting the Environment Variable path
--------------------------------------

While installing python, an option to set the environment variable path will be
available. This is optional but if you choose to set it, you can run
Python from the cmd simply by opening your command prompt and
typing:

.. code:: console
    
    C:\Users\YourUserName>python

Then you should see something similar appear:

.. code:: console
    
    C:\Users\YourUserName>python
    Python 2.7.17 (v2.7.17:c2f86d86e6, Oct 19 2019, 20:49:36) [MSC v.1500 32 bit (Intel)] on win32
    Type "help", "copyright", "credits" or "license" for more information.
    >>>

Now you are ready to program in python.

If you choose to not set the path through the installation, you can do it
manually by navigating to the following location:

.. code:: console
    
    Control Panel >> System (Under System and Security if not in icon mode) >> Advanced
    System Settings >> Environment Variables

Once you are at the Environment Variables window, you should have a User variable
called Path. Either select it and hit the Edit button or double click it.
Once the edit window opens, hit the Edit button or double click an empty line
and type:

.. code:: console

    C:\Python27

or

.. code:: console

    C:\path\to\your\python\here

For the official Python documentation visit: `Python Documentation 
<https://docs.python.org/2/index.html>`_

NAOqi for Python
--------------------------------------
Once python is installed, you need to use Aldebaran's NAOqi framework.

Visit `SoftBank Robotics Community <https://community.ald.softbankrobotics.com/en/resources/software/language/en-gb>`_ 
and download **[FORMER NAO VERSIONS] - Python NAOqi SDK**. I suggest doing the setup download
as you will get the exe file.

Once you begin your installation, you will arrive at this point:

.. image:: ./imgs/pynaoqi.png
    :width: 500px
    :align: center
    :height: 350px

If you have multiple versions of python installed, make sure you have
selected the **correct version of python (python 2.7)** otherwise you
will get import issues.

After the installation is done, it will allow you to do

.. code:: console

    import naoqi
    import qi

in your python files.

If these lines run without any issues, we are ready for our first tutorial.


RoboWalknTalk
--------------------------------------
This tutorial follows parts of examples from the documentation of NAO.

For more information, see: `NAOqi's Developer Guide <http://doc.aldebaran.com/2-8/index_dev_guide.html>`_

Open your favorite Python IDE. Run the following lines of code:

.. code-block:: python
    :linenos:

    from naoqi import ALProxy
    import qi
    roboIP = "NAO_IP"   # Example: roboIP = "172.18.72.172"
    roboPort = 9559     # This is by default
    motion = ALProxy("ALMotion", roboIP, roboPort)
    tts = ALProxy("ALTextToSpeech", roboIP, roboPort)

Lines 1-2 are importing the NAOqi for Python framework.
For line 3 you need to set your NAO ip address. You can obtain the IP of NAO by simply pressing the button on its chest.
For line 4, we are setting up the default port that NAO uses to communicate with your PC.

Line 5 and 6, the `ALProxy <http://doc.aldebaran.com/2-8/dev/python/making_nao_speak.html?highlight=alproxy#ALProxy>`_ 
object lets you create a proxy to a module. This lets you use 
`ALMotion  <http://doc.aldebaran.com/2-8/naoqi/motion/almotion-api.html#almotion-api>`_ and 
`ALTextToSpeech <http://doc.aldebaran.com/2-4/naoqi/audio/altexttospeech-api.html>`_.

The next part of our script is:

.. code-block:: python
    :lineno-start: 7

    # Stiffness value has to be greater than 0
    motion.moveInit()
    motion.setStiffnesses("Body", 1.0)
    motion.post.moveTo(0.5,0,0)
    tts.say("I can walk!")
    tts.say("I can do multiple thigns at the same time as well.")
    tts.say("It is only a matter of time before I take over the world!")
    motion.moveTo(0,0, 1.5709*2)
    motion.post.moveTo(0.5,0,0)
    tts.say("I can even turn around on my own!")

First off, we are required to have NAO in the correct position to walk. We use the function moveInit() in line 8 to do this.

Second, the robot will not walk unless the stiffness levels are set. That is, any value greater than 0 will work.
We do this in line 9 by using the function setStiffness("Body", 1.0) where the value 1.0 is the stiffness level.

In line 10, we make use of the post method. This method will allow NAO to speak and walk at the same time through synchronous
programming. For more information, visit `Blocking and Non-Blocking Calls <http://doc.aldebaran.com/2-8/dev/naoqi/index.html#naoqi-blocking-non-blocking>`_.
We also use the `moveTo() <http://doc.aldebaran.com/1-14/naoqi/motion/control-walk-api.html#ALMotionProxy::moveTo__floatCR.floatCR.floatCR>`_ 
method to make NAO walk.

In short, moveTo(x, y, z) means x and y are coordinate planes where the front of NAO is the positive
x axis and the right arm direction is the positive y axis. The z variable tells NAO how much to rotate.

As NAO continues to walk, it will continue to run lines 11-13. The say(string) function tells NAO what to say.

Once NAO finishes walking, it will execute line 14. The value 1.5709*2 is simply pi, telling NAO to rotate pi, or 180, degrees.

Line 15 tells NAO to move again 0.5 meters and as NAO walks, line 16 will execute.

For other variations such as having NAO talk first then walk or vice versa, visit
this `site <http://doc.aldebaran.com/2-8/dev/python/making_nao_move.html#making-nao-move-and-speak-at-the-same-time>`_.

You have now completed your first script! I suggest visiting the NAOqi framework website to learn more and create other interesting
scripts.